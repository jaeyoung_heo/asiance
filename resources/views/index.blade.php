@extends('layouts.index')
@include('library.vuejs')
@push('pre-styles')
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{asset('inspinia/css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">
{{--    <link href="{{asset('plugin/lightbox/dist/css//lightbox.min.css')}}" rel="stylesheet">--}}


@endpush
@push('post-styles')
    <style>
        .author_title {
            font-size: 1.2em;
            font-weight: bolder;
        }

        .post-title {
            font-size: 25px;
            font-weight: bolder;
        }

        .post-timestamp {
            color: #e2e2e2
        }

        .post_list {
        }

        .post-item {
            background: #FFF;
            margin: 10px;
            padding: 20px;
        }
    </style>
@endpush
@section('content')


    <div class="col-md-12 post_list" id="post_list">
        <div class="row">
            <div class="col-md-12 post-item" v-for="(post_item, index) in post">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class=" float-left">
                            <div class="post-title">@{{ post_item.title }}</div>
                            <div class="post-timestamp">@{{ post_item.created_at }}</div>
                            <div class="post-timestamp">@{{ post_item.updated_at }}</div>
                            <div class="post-tag"><i class="fa fa-tags"></i> @{{ post_item.tags }}
                            </div>
                        </div>
                        <div class="float-right">
                            <div class="float-left">
                                <div :style="{ 'background-image': 'url(' + post_item.author.avatar + ')' }" style="margin:10px;width:80px; min-height: 80px;  background-size: cover; background-position: center; border-radius: 100%"></div>
                            </div>
                            <div class="float-left">
                                <div class="author_title">@{{ post_item.author.name }}</div>
                                <div class="author_role">@{{ post_item.author.role }}</div>
                                <div class="author_location">@{{ post_item.author.location }}</div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12 mt-4">
                        <div class="row">
                            <div class="lightBoxGallery">
                                <a :href="post_item.image_url" title="Image from Unsplash" data-gallery=""><img class="col-md-4 img-fluid"  :src="post_item.image_url"></a>
                            </div>

                            <div class="col-md-8" v-html="post_item.body" ></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center">

                <nav aria-label="Page navigation ">
                    <ul class="pagination ">
                        <li class="mr-2">

                        </li>
                        <li class="page-item">
                            <a class="page-link" href="javascript:void(0)" v-on:click="updateNavigation('first')" :data-id="1"><<</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="javascript:void(0)" v-on:click="updateNavigation('prev')" :data-id="navigation.previous"><</a>
                        </li>
                        <li v-for="index in  navigation.page_range" v-bind:class="{'page-item':true, 'active':(navigation.page_display_first +index -1 === navigation.current_page)}">
                            <a class="page-link" href="javascript:void(0)" v-on:click="updateNavigation" :data-id="navigation.page_display_first +index -1">@{{ navigation.page_display_first +index -1 }}</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="javascript:void(0)" v-on:click="updateNavigation('next')" :data-id="navigation.current_page +1">></a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="javascript:void(0)" v-on:click="updateNavigation('last')" :data-id="navigation.last_page">>></a>
                        </li>
                    </ul>
                </nav>
            </div>

            <div id="blueimp-gallery" class="blueimp-gallery">
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>
        </div>
    </div>

@endsection
@push('pre-scripts')
    <script src="{{asset('inspinia/js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>

    <script src="{{asset('plugin/lightbox/dist/js/lightbox.min.js')}}"></script>
@endpush
@push('post-scripts')
    <script>

        var app = new Vue({
            el: '#post_list',
            data: {
                page:1,
                post: [],
                navigation: {}

            },
            methods:{
                updatePage:function(request_page)
                {
                    $.ajax({
                        url: '{{route("get_post")}}',
                        type: 'post',
                        data: {
                            _token: "{{ csrf_token() }}",
                            page : request_page
                        },
                        success: function (data) {
                            app.post = data.post.data;
                            app.setNavigation(data.post);
                        }
                    });
                },
                updateNavigation: function (index) {
                    var request_page = 1;
                    switch (index) {
                        case"next":
                            request_page = this.navigation.current_page + 1;
                            if (request_page > this.navigation.last_page)
                                request_page = this.navigation.last_page;
                            break;
                        case "last":
                            request_page = this.navigation.last_page;
                            break;
                        case "first":
                            request_page = 1;
                            break;
                        case "prev":
                            request_page = this.navigation.current_page - 1;
                            if (request_page < 1)
                                request_page = 1;

                            break;
                        default:
                            request_page = $(index.target).data("id");
                            break;
                    }
                    this.updatePage(request_page);

                },
                setNavigation: function (data) {

                    var page_display_last = Math.ceil(data.current_page / 10) * 10;
                    var page_display_first = page_display_last - 10 + 1;

                    if (page_display_last > data.last_page) {
                        page_display_last = data.last_page;
                    }
                    var previous = data.current_page - 1;
                    var next = data.current_page + 1;

                    if (previous < 1)
                        previous = 1;

                    if (next > data.last_page)
                        next = data.last_page;

                    this.navigation = {
                        current_page: data.current_page,
                        from: data.from,
                        last_page: data.last_page,
                        path: data.path,
                        per_page: data.per_page,
                        to: data.to,
                        total: data.total,
                        page_display_first: page_display_first,
                        page_display_last: page_display_last,
                        page_range: page_display_last - page_display_first + 1,
                        previous: previous,
                        next: next,
                    };
                },
            },
            mounted: function () {
                this.updatePage(1);
            },
        });
        $(function () {
            // $("#lightgallery").lightGallery();
        })


    </script>

@endpush