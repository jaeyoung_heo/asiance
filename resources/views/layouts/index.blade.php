<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>The Blog</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    @stack('pre-styles')
    <style>
        .title{
            font-size: 30px;
            font-weight: bolder;
        }
        .active
        {
            color: #000 !important;
            font-weight: 400;
            border-bottom: 1px solid #000000;

        }
        .sub-menu {
            margin: 0px;
            padding: 0px;
        }
        .sub-menu li{
            list-style: none;
            text-align: center;
            display: inline-block;
            color: #aaaaaa ;
            margin: 10px;
        }
    </style>
    @stack('post-styles')
</head>
<body style="background: #f6f6f6;">
<header style="background:#fff">
<div class="col-md-12 text-center title " >
    THE BLOG
</div>
<div class="col-md-12 text-center">
    <ul class="sub-menu">
        <li class="active">POSTS</li>
        <li>ABOUT US</li>
    </ul>
</div>
</header>
<div class="container">
    <div class="row">

        <div >
        @yield('content')
        </div>
    </div>
</div>
<!-- Scripts -->
<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

@stack('pre-scripts')
@stack('post-scripts')

</body>
</html>
