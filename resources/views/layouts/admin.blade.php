<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Main view</title>

    <link href="{{asset('inspinia/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('inspinia/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('inspinia/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('inspinia/css/style.css')}}" rel="stylesheet">
    @include('library/loader')
    @include('library/toastr')
    @include('library/sweetAlert')
    <style>
        .help-block{
            color:red;
        }
    </style>
    @stack('pre-styles')
    @stack('post-styles')
</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="block m-t-xs font-bold">Example user</span>
                            <span class="text-muted text-xs block">menu <b class="caret"></b></span>
                        </a>
{{--                        <ul class="dropdown-menu animated fadeInRight m-t-xs">--}}
{{--                            <li><a class="dropdown-item" href="{{route("logout")}}">Logout</a></li>--}}
{{--                        </ul>--}}
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li>
                    <a href="#"><i class="fa fa-product-hunt"></i> <span class="nav-label">Author</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('list_author')}}">Author List</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-product-hunt"></i> <span class="nav-label">Post</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('list_post')}}">Post List</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
{{--                        <a href="/logout">--}}
{{--                            <i class="fa fa-sign-out"></i> Log out--}}
{{--                        </a>--}}
                    </li>
                </ul>

            </nav>
        </div>

        @yield('content')


        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2018
            </div>
        </div>

    </div>
</div>

<!-- Mainly scripts -->
<script src="{{asset('inspinia/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('inspinia/js/popper.min.js')}}"></script>
<script src="{{asset('inspinia/js/bootstrap.min.js')}}"></script>
<script src="{{asset('inspinia/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('inspinia/js/inspinia.js')}}"></script>
<script src="{{asset('inspinia/js/plugins/pace/pace.min.js')}}"></script>
<script>
    $.fn.extend({
        autofill: function autofill(data, options) {
            var settings = {
                    findbyname: true,
                    restrict: true
                },
                self = this;

            if (options) {
                $.extend(settings, options);
            }

            return this.each(function () {
                $.each(data, function (k, v) {

                    // switch case findbyname / findbyid

                    var selector, elt;

                    if (settings.findbyname) {
                        // by name

                        selector = '[name="' + k + '"]';
                        elt = settings.restrict ? self.find(selector) : $(selector);

                        if (elt.length == 1) {
                            elt.val(elt.attr("type") == "checkbox" ? [v] : v);
                        } else if (elt.length > 1) {
                            // radio
                            elt.val([v]);
                        } else {
                            selector = '[name="' + k + '[]"]';
                            elt = settings.restrict ? self.find(selector) : $(selector);
                            elt.each(function () {
                                $(this).val(v);
                            });
                        }
                    } else {
                        // by id

                        selector = '#' + k;
                        elt = settings.restrict ? self.find(selector) : $(selector);

                        if (elt.length == 1) {
                            elt.val(elt.attr("type") == "checkbox" ? [v] : v);
                        } else {
                            var radiofound = false;

                            // radio
                            elt = settings.restrict ? self.find('input:radio[name="' + k + '"]') : $('input:radio[name="' + k + '"]');
                            elt.each(function () {
                                radiofound = true;
                                if (this.value == v) {
                                    this.checked = true;
                                }
                            });
                            // multi checkbox
                            if (!radiofound) {
                                elt = settings.restrict ? self.find('input:checkbox[name="' + k + '[]"]') : $('input:checkbox[name="' + k + '[]"]');
                                elt.each(function () {
                                    $(this).val(v);
                                });
                            }
                        }
                    }
                });
            });
        }
    });
</script>
@stack('pre-scripts')
@stack('post-scripts')
</body>

</html>
