@extends('layouts.admin')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Author<h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route("home")}}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Author</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="col-md-12">
                            <a href="{{route("edit_author")}}" class="btn btn-primary">Add</a>
                            <table class="table table-striped table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Author Name</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                    <th></th>
                                </tr>
                                @foreach($authors as $author)
                                <tr >
                                    <td>{{$author->id}}</td>
                                    <td>{{$author->name}}</td>
                                    <td>{{$author->created_at}}</td>
                                    <td>{{$author->updated_at}}</td>
                                    <td><a class="btn btn-primary" href="{{route("edit_author",["id"=>$author->id])}}">Edit</a></td>
                                </tr>
                                @endforeach
                            </table>

                            {{$authors->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection