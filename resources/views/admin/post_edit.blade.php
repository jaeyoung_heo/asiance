@extends('layouts.admin')
@include('library.jForm')
@include('library.tinymce')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Post</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Post List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="col-md-12">
                            <form method="post" action="{{route("post_post")}}" id="frmEdit">
                                @csrf
                                <input type="hidden" name="id" value="{{$post->id}}">

                                <div class="form-group">
                                    <label for="title">Author</label>
                                    <select class="form-control" name="author">
                                        @foreach($authors as $author)
                                            <option value="{{$author->name}}">{{$author->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" id="title" placeholder="Title" name="title" autocomplete="off" value="{{$post->title}}" required="required">
                                </div>

                                <div class="form-group">
                                    <label for="body">Body</label>
                                    <textarea class="editable" name="body">{!! $post->body !!}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="tags">Tag</label>
                                    <input type="text" class="form-control" id="tags" placeholder="Tag" name="tags" autocomplete="off" value="{{$post->tags}}" >
                                </div>

                                <div class="form-group">
                                    <label for="image_url">Image URL</label>
                                    <input type="text" class="form-control" id="image_url" placeholder="Image URL" name="image_url" autocomplete="off" value="{{$post->image_url}}" required="required">
                                </div>

                            @if($post)
                                    <button class="btn btn-primary">Edit</button>
                                @else
                                    <button class="btn btn-primary">Add</button>
                                @endif
                                <a class="btn btn-danger" href="{{route("list_post")}}">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@push('post-scripts')
    <script>
        $(function () {

            $("#frmEdit").ajaxForm({
                type: 'post',
                beforeSubmit: function () {

                    loadingData();
                },
                success: function (data) {
                    if (data.is_success) {
                        swal.fire("성공", data.msg, "success").then(function () {
                            location.href = '{{route("list_post")}}';
                        })
                    } else {
                        swal.fire("실패", data.msg, "error");
                    }
                },
                complete: function (data) {
                    loadingData('stop');
                }
            });

        })
    </script>
@endpush