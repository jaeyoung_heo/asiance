@extends('layouts.admin')
@include('library.jForm')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Author</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Author List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="col-md-12">
                            <form method="post" action="{{route("post_author")}}" id="frmEdit">
                                @csrf
                                <input type="hidden" name="id" value="{{$author->id}}">

                                <div class="form-group">
                                    <label for="title">Name</label>
                                    <input type="text" class="form-control" id="name" placeholder="Name" name="name" autocomplete="off" value="{{$author->name}}" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    <input type="text" class="form-control" id="role" placeholder="Role" name="role" autocomplete="off" value="{{$author->role}}" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="avatar">Avatar</label>
                                    <input type="text" class="form-control" id="avatar" placeholder="Avatar" name="avatar" autocomplete="off" value="{{$author->avatar}}" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <input type="text" class="form-control" id="location" placeholder="Location" name="location" autocomplete="off" value="{{$author->location}}" required="required">
                                </div>


                                @if($author)
                                    <button class="btn btn-primary">Edit</button>
                                @else
                                    <button class="btn btn-primary">Add</button>
                                @endif
                                <a class="btn btn-danger" href="{{route("list_author")}}">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@push('post-scripts')
    <script>
        $(function () {

            $("#frmEdit").ajaxForm({
                type: 'post',
                beforeSubmit: function () {

                    loadingData();
                },
                success: function (data) {
                    if (data.is_success) {
                        swal.fire("성공", data.msg, "success").then(function () {
                            location.href = '{{route("list_author")}}';
                        })
                    } else {
                        swal.fire("실패", data.msg, "error");
                    }
                },
                complete: function (data) {
                    loadingData('stop');
                }
            });

        })
    </script>
@endpush