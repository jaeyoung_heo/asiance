@push('post-scripts')
    <script src="/plugin/loading-master/jquery.loading.min.js"></script>
    <script>
        var loadingData = function (option) {
            $('body').loading(option);
            switch (option == "") {
                case "":
                    $('input textarea').attr("disabled","disabled");
                    break;
                case "stop":
                    $('input textarea').removeAttr("disabled","disabled");
                    break;
            }

        }
    </script>
@endpush

@push('pre-styles')
    <link href="/plugin/loading-master/jquery.loading.min.css" rel="stylesheet">
@endpush