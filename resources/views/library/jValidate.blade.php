@push("pre-scripts")
{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>--}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/additional-methods.min.js"></script>--}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/additional-methods.min.js"></script>
<script>
    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');//.removeClass("has-success");
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');//.addClass("has-success");
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.closest('.input-group').length) {
                error.insertAfter(element.closest('.input-group').first());
            } else {
                error.insertAfter(element);
            }
        }
    });
    $.validator.addMethod("email",
        function(value, element) {
            return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
        },
        "Please enter a valid email address."
    );
</script>
@endpush
@push("pre-styles")
    <style>
        .help-block
        {
            color: red;
        }
    </style>
    @endpush