@include('library.fileManager')


@push("pre-scripts")
    {{--<script src="https://cdn.tiny.cloud/1/0zfur0x5bubzobdyjyh6gd2buhgz49kzn9gkdevho9f1ngr7/tinymce/5/tinymce.min.js"></script>--}}
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush

@push('post-scripts')
    <script>
        $(function () {

            var editor_config = {
                selector: 'textarea.editable',
                menubar: false,
                plugins: "link code image",
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify |' +
                    ' bullist numlist outdent indent | link image | print preview media fullpage | ' +
                    'forecolor backcolor emoticons | help | code  removeformat',
                path_absolute: "/",

                setup: function (editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                },
                relative_urls: false,
                height: 200,

            };

            tinymce.init(editor_config);
        })
    </script>
@endpush