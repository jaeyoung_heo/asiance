<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 * @package App\Models
 * @property integer $id
 * @property string $author
 * @property string $title
 * @property string $body
 * @property string $tags
 * @property string $image_url
 */
class Post extends Model
{

    public function author(){
        return $this->belongsTo(Author::class,'author','name');
    }
}
