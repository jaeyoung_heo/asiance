<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Author
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $role
 * @property string $avatar
 * @property string $location
 */
class Author extends Model
{
}
