<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    const PAGE_PER = 5;

    public function index() {
        return view('index');
    }


    public function getPost(Request $request) {

        $page = $request->get('page');
        $post = Post::with('author')->paginate(SELF::PAGE_PER, ['*'], "page", $page);
        $data = [
            "post" => $post
        ];
        return response()->json($data, 200);
    }

    public function apiPost(Request $request) {
        $author = $request->get('author');

        $post = $author ? Post::where('author', $author)->get() : Post::all();

        return response()->json(['author' => $post], 200);
    }

    public function apiAuthor(Request $request) {
        $name = $request->get('name');

        $author = $name ? Author::where('name', $name)->get() : Author::all();

        return response()->json(['author' => $author], 200);
    }
}
