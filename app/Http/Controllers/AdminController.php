<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function listAuthor() {
        $authors = Author::orderBy('updated_at', 'desc')->paginate(10);
        $data = [
            "authors" => $authors
        ];
        return view('admin.author_list', $data);
    }

    public function editAuthor($id = null) {


        if (!empty($id)) {
            $author = Author::find($id);
        } else {
            $author = new Author();
        }
        $data = [
            "author" => $author,
        ];

        return view('admin.author_edit', $data);
    }

    public function postAuthor(Request $request) {
        try {
            DB::transaction(function () use ($request) {

                $id = $request->get('id');
                $name = $request->get('name');
                $role = $request->get('role');
                $avatar = $request->get('avatar');
                $location = $request->get('location');

                if ($id) {
                    $author = Author::find($id);

                } else {
                    $author = new Author();
                }
                $author->name = $name;
                $author->role = $role;
                $author->avatar = $avatar;
                $author->location = $location;
                $author->save();

            });
        } catch (\Exception $exception) {
            $data = [
                "is_success" => false,
                'msg' => $exception->getMessage(),
            ];
            return response()->json($data);
        }
        $data = [
            "is_success" => true,
            'msg' => "Successfully Saved",
        ];
        return response()->json($data);
    }
    public function listPost() {
        $posts = Post::orderBy('updated_at', 'desc')->paginate(10);
        $data = [
            "posts" => $posts
        ];
        return view('admin.post_list', $data);
    }

    public function editPost($id = null) {


        if (!empty($id)) {
            $post = Post::find($id);
        } else {
            $post = new Post();
        }

        $data = [
            "post" => $post,
            "authors"=> Author::all()
        ];

        return view('admin.post_edit', $data);
    }
    public function postPost(Request $request) {
        try {
            DB::transaction(function () use ($request) {

                $id = $request->get('id');
                $author = $request->get('author');
                $title = $request->get('title');
                $body = $request->get('body');
                $tags = $request->get('tags');
                $image_url = $request->get('image_url');

                if ($id) {
                    $post = Post::find($id);

                } else {
                    $post = new Post();
                }
                $post->author = $author;
                $post->title = $title;
                $post->body = $body;
                $post->tags = $tags;
                $post->image_url = $image_url;
                $post->save();

            });
        } catch (\Exception $exception) {
            $data = [
                "is_success" => false,
                'msg' => $exception->getMessage(),
            ];
            return response()->json($data);
        }
        $data = [
            "is_success" => true,
            'msg' => "Successfully Saved",
        ];
        return response()->json($data);
    }
}
