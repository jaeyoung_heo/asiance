<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@listAuthor')->name('list_author');
    Route::get("/author/edit/{id?}", "AdminController@editAuthor")->name("edit_author");
    Route::post('/author/edit', 'AdminController@postAuthor')->name('post_author');

    Route::get('/post', 'AdminController@listPost')->name('list_post');
    Route::get("/post/edit/{id?}", "AdminController@editPost")->name("edit_post");
    Route::post('/post/edit', 'AdminController@postPost')->name('post_post');
});

Route::post('/get_post',"HomeController@getPost")->name('get_post');
Route::get('/api/post',"HomeController@apiPost")->name('api_post');
Route::get('/api/author',"HomeController@apiAuthor")->name('api_author');